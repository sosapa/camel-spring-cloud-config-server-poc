# README #

POC for creating spring-cloud config server

### What is this repository for? ###

* Proof of concept of Spring cloud config server

## References

* https://spring.io/guides/gs/centralized-configuration/
* https://cloud.spring.io/spring-cloud-config/
* https://cloud.spring.io/spring-cloud-config/multi/multi__quick_start.html
* https://spring.io/guides/gs/centralized-configuration/

* https://docs.spring.io/spring-boot/docs/current/reference/html/getting-started-installing-spring-boot.html
* https://cloud.spring.io/spring-cloud-cli/

### How do I get set up? ###

