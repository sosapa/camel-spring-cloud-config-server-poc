// Copyright 2018 Red Energy
// Pablo Sosa (psosa_ar@yahoo.com)
// 2018-10-10

package au.com.redenergy.springcloudconfig

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.config.server.EnableConfigServer

@EnableConfigServer
@SpringBootApplication
class Application  {

  static void main(String[] args) {
    SpringApplication.run(Application, args)
  }

}
